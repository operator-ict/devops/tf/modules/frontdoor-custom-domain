variable "name" {
  type = string
}

variable "frontdoor_profile_id" {
  type = string
}

variable "dns_zone_id" {
  type = string
}

variable "dns_zone_name" {
  type = string
}

variable "resource_group_name" {
  type = string
}

variable "domain_name" {
  type = string
}

variable "frontdoor_endpoint_hostname" {
  type = string
}
variable "frontdoor_endpoint_id" {
  type = string
}

variable "is_dns_zone" {
  type = bool
}

variable "dns_auth" {
  type = bool
}

variable "dns_traffic" {
  type = bool
}
