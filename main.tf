resource "azurerm_cdn_frontdoor_custom_domain" "domain" {
  name                     = trim(replace(replace(var.name, ".", "-"), "/[^a-zA-Z0-9-]/", ""), "-")
  cdn_frontdoor_profile_id = var.frontdoor_profile_id
  dns_zone_id              = var.dns_zone_id
  host_name                = var.name

  tls {
    certificate_type    = "ManagedCertificate"
    minimum_tls_version = "TLS12"
  }
}

resource "azurerm_dns_txt_record" "txt" {
  count               = var.dns_auth ? 1 : 0
  name                = var.is_dns_zone ? "_dnsauth" : join(".", ["_dnsauth", var.domain_name])
  zone_name           = var.dns_zone_name
  resource_group_name = var.resource_group_name
  ttl                 = 3600

  record {
    value = azurerm_cdn_frontdoor_custom_domain.domain.validation_token
  }
}

resource "azurerm_dns_cname_record" "cname" {
  count               = var.dns_traffic && !var.is_dns_zone ? 1 : 0
  name                = var.domain_name
  zone_name           = var.dns_zone_name
  resource_group_name = var.resource_group_name
  ttl                 = 3600
  record              = var.frontdoor_endpoint_hostname
}

resource "azurerm_dns_a_record" "a" {
  count               = var.dns_traffic && var.is_dns_zone ? 1 : 0
  name                = "@"
  zone_name           = var.dns_zone_name
  resource_group_name = var.resource_group_name
  ttl                 = 3600
  target_resource_id  = var.frontdoor_endpoint_id
}
